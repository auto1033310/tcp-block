#include <stdio.h>
#include <pcap.h>
#include <libnet.h>
#include <regex>
#include <string>
#include <fstream>
#include <iostream>
#include <sys/socket.h>

#define MAC_ADDR_LEN 6
#define ipv4_type 0x800
#define TCP_protocol 0x6
#define HTTP_dp 80
#define redirect_to_warning "HTTP/1.0 302 Redirect\r\nLocation: http://warning.or.kr\r\n\r\n"

using namespace std;

void usage(){

    printf("syntax : tcp-block <interface> <pattern>\n");
    printf("sample : tcp-block wlan0 \"Host: test.gilgil.net\"\n");

}

typedef struct {
    char* dev;

} Param;

Param param={
    .dev=NULL

};


char malicious[200];
int mal_size;


typedef struct{
    struct libnet_ethernet_hdr e;
    struct libnet_ipv4_hdr I;
    struct libnet_tcp_hdr T;
    char http_redirct[strlen(redirect_to_warning)];


} http_pkt;

typedef struct{
    struct libnet_ethernet_hdr *e;
    struct libnet_ipv4_hdr *I;
    struct libnet_tcp_hdr *T;
    char* http_hdr;
} http_pkt_pos;

int size_till_tcp=sizeof(libnet_ethernet_hdr)+sizeof(libnet_ipv4_hdr)+sizeof(libnet_tcp_hdr);

bool check(int argc){
    if(argc <3){
        usage();
        return false;
    }
    return true;

}

void get_mac_addr(const string& if_name, uint8_t* mac_addr){


    ifstream iface("/sys/class/net/"+if_name+"/address");
    string str((istreambuf_iterator<char>(iface)), istreambuf_iterator<char>());

    if(str.length()>0){
        string hex = regex_replace(str, regex(":"),"");
        uint64_t res = stoull(hex,0,16);
        for(int i=0;i<MAC_ADDR_LEN;i++){
            mac_addr[MAC_ADDR_LEN-i-1]=(uint8_t)((res& ((uint64_t)0xff<<(i*8))) >> (i*8));
        }
        printf("MY MAC Address: ");
        for(int i=0;i<MAC_ADDR_LEN-1;i++){
            printf("%02X:", mac_addr[i]);
        }
        printf("%02X\n", mac_addr[MAC_ADDR_LEN-1]);

        return;

    }

    printf("Can't get Mac addr");
}

bool check_if_malicious(char* http_hdr, int size){
    int i;

    for(i=0;i<size;i++){
        if(!strncmp(http_hdr+i, "Host: ", 6)){
            printf("has host\n");
            if(!strncmp(http_hdr+i, malicious, mal_size)) return true;
            return false;
        }
    }
    return false;
}

uint16_t warp_around_sum(uint32_t a, uint16_t b){
    a+=b;
    a+=(a>>16);
    a&=0xffff;
    return a;
}

uint16_t calc_ip_checksum(libnet_ipv4_hdr* I){
    I->ip_sum=0;
    uint32_t sum=0;
    uint16_t *pt= (uint16_t*)I;

    for(int i=0;i<10;i++){

        sum+=*(uint16_t*)(pt+i);
    }
    sum+=(sum>>16);
    sum&=0xffff;
    sum=0xffff-sum;
    return sum;
}

uint16_t calc_tcp_checksum(http_pkt* h){
    h->T.th_sum=0;
    uint32_t pseudo_sum=0;
    uint32_t segment_sum=0;
    uint32_t tcp_size=sizeof(h->T)+strlen(h->http_redirct);
    uint16_t *pt=(uint16_t*)(&h->I.ip_src);
    uint32_t i;
    for(i=0;i<4;i++){
        pseudo_sum=warp_around_sum(pseudo_sum, *(uint16_t*)(pt+i));
    }

    pseudo_sum=warp_around_sum(pseudo_sum, h->I.ip_p);
    pseudo_sum=warp_around_sum(pseudo_sum, tcp_size);

    pt=(uint16_t*)(&h->T);
    for(i=0;i<tcp_size;i++){
        segment_sum=warp_around_sum(segment_sum, *(uint16_t*)(pt+i));
    }

    uint16_t sum=warp_around_sum(pseudo_sum, segment_sum);

    return ~sum;

}

void make_forward_pkt(http_pkt* fwd_pkt,http_pkt_pos* pos_struct, uint8_t* my_mac, int packet_size){

    memcpy(fwd_pkt->e.ether_shost, my_mac, MAC_ADDR_LEN);
    memcpy(fwd_pkt->e.ether_dhost, pos_struct->e->ether_dhost, MAC_ADDR_LEN);
    fwd_pkt->e.ether_type=ipv4_type;

    fwd_pkt->I.ip_v=4;      //ipv4
    fwd_pkt->I.ip_hl=5;     //ip header length=20
    fwd_pkt->I.ip_tos=0;
    fwd_pkt->I.ip_len= htons(sizeof(libnet_ipv4_hdr)+sizeof(libnet_tcp_hdr));   //total length(fin)
    fwd_pkt->I.ip_id=pos_struct->I->ip_id;      //same identification
    fwd_pkt->I.ip_off=0;                        //no fragment
    fwd_pkt->I.ip_ttl=pos_struct->I->ip_ttl;    //same ttl
    fwd_pkt->I.ip_p=TCP_protocol;
    memcpy(&fwd_pkt->I.ip_src, &pos_struct->I->ip_src, sizeof(in_addr));
    memcpy(&fwd_pkt->I.ip_dst, &pos_struct->I->ip_dst, sizeof(in_addr));
    fwd_pkt->I.ip_sum=calc_ip_checksum(&fwd_pkt->I);

    fwd_pkt->T.th_sport=pos_struct->T->th_sport;
    fwd_pkt->T.th_dport=pos_struct->T->th_dport;
    fwd_pkt->T.th_seq=htonl(ntohl(pos_struct->T->th_seq)+packet_size-size_till_tcp);
    fwd_pkt->T.th_ack=pos_struct->T->th_ack;
    fwd_pkt->T.th_off=5;
    fwd_pkt->T.th_x2=0;
    fwd_pkt->T.th_flags=TH_RST;
    fwd_pkt->T.th_flags|=TH_ACK;
    fwd_pkt->T.th_win=pos_struct->T->th_win;
    memset(fwd_pkt->http_redirct, 0, strlen(redirect_to_warning));
    fwd_pkt->T.th_sum=calc_tcp_checksum(fwd_pkt);

}

void make_backward_pkt(http_pkt* bwd_pkt,http_pkt_pos* pos_struct, uint8_t* my_mac, bool is_https, int packet_size){

    memcpy(bwd_pkt->e.ether_shost, my_mac, MAC_ADDR_LEN);
    memcpy(bwd_pkt->e.ether_dhost, pos_struct->e->ether_shost, MAC_ADDR_LEN);
    bwd_pkt->e.ether_type=ipv4_type;

    bwd_pkt->I.ip_v=4;
    bwd_pkt->I.ip_hl=5;
    bwd_pkt->I.ip_tos=0;
    bwd_pkt->I.ip_len= htons(sizeof(libnet_ipv4_hdr)+sizeof(libnet_tcp_hdr));
    bwd_pkt->I.ip_id=pos_struct->I->ip_id+0x100;    // +1 identification
    bwd_pkt->I.ip_off=0;
    bwd_pkt->I.ip_ttl=128;
    bwd_pkt->I.ip_p=TCP_protocol;
    memcpy(&bwd_pkt->I.ip_src, &pos_struct->I->ip_dst, sizeof(in_addr));
    memcpy(&bwd_pkt->I.ip_dst, &pos_struct->I->ip_src, sizeof(in_addr));
    bwd_pkt->I.ip_sum=calc_ip_checksum(&bwd_pkt->I);

    bwd_pkt->T.th_sport=pos_struct->T->th_dport;
    bwd_pkt->T.th_dport=pos_struct->T->th_sport;
    bwd_pkt->T.th_seq=pos_struct->T->th_ack;
    bwd_pkt->T.th_ack=htonl(ntohl(pos_struct->T->th_seq) +packet_size-size_till_tcp);

    uint32_t pos_data_size=packet_size-size_till_tcp;
    bwd_pkt->T.th_ack=htonl(ntohl(pos_struct->T->th_seq)+pos_data_size);
    if(is_https){
        bwd_pkt->T.th_flags=TH_RST;
        bwd_pkt->T.th_flags|=TH_ACK;
        bwd_pkt->T.th_win=pos_struct->T->th_win;
        bwd_pkt->T.th_urp=0;
        memset(bwd_pkt->http_redirct, 0, strlen(redirect_to_warning));
        bwd_pkt->T.th_sum=calc_tcp_checksum(bwd_pkt);
    }
    else{
        bwd_pkt->T.th_flags=TH_FIN;
        bwd_pkt->T.th_flags|=TH_ACK;
        bwd_pkt->T.th_win=pos_struct->T->th_win;
        strncpy(bwd_pkt->http_redirct, redirect_to_warning, strlen(redirect_to_warning));
        bwd_pkt->T.th_urp=0;
        bwd_pkt->T.th_sum=calc_tcp_checksum(bwd_pkt);
    }


}

int send_rawsocket(http_pkt* packet, int sockfd){
    int res;
    struct sockaddr_in sockaddr;
    sockaddr.sin_family=AF_INET;
    sockaddr.sin_port=packet->T.th_sport;
    memcpy(&sockaddr.sin_addr.s_addr, &packet->I.ip_src, sizeof(in_addr));


    res=sendto(sockfd, packet, sizeof(*packet), 0, (struct sockaddr*)&sockaddr, sizeof(sockaddr));
    return res;
}

void dump(const  u_char* buf, int size) {
    int i;
    printf("\n");
    for (i = 0; i < size; i++) {
        if (i != 0 && i % 16 == 0)
            printf("\n");
        printf("%02X ", buf[i]);
    }
    printf("\n");
}


bool parse_if_http_malicious(http_pkt_pos* h, const u_char* packet, int packet_size){
    //dump(packet, packet_size);
    struct libnet_ethernet_hdr *e;
    e=(struct libnet_ethernet_hdr*)packet;
    if(e->ether_type!=htons(ipv4_type)) return false; //not ipv4

    struct libnet_ipv4_hdr *I;
    I=(struct libnet_ipv4_hdr*)(packet+sizeof(*e));
    if(I->ip_p !=TCP_protocol) return false; //not tcp

    struct libnet_tcp_hdr *T;
    T=(struct libnet_tcp_hdr*)(packet+sizeof(*e)+4*I->ip_hl);
    if(ntohs(T->th_dport)!=HTTP_dp) return false; //not http
    //printf("http\n");
    int http_pos;
    char* http_hdr;
    http_pos=sizeof(*e)+4*I->ip_hl +sizeof(*T);
    http_hdr=(char*)(packet+http_pos);

    if(!check_if_malicious(http_hdr, packet_size-http_pos)) return false;
    printf("has malicious\n");
    h->e=e;
    h->I=I;
    h->T=T;
    h->http_hdr=http_hdr;

    return true;

}

int main(int argc, char** argv){

    if(!check(argc)) return -1;

    param.dev=argv[1];

    mal_size=(int)strlen(argv[2]);

    memcpy(malicious, argv[2], mal_size);

    int sockfd;
    sockfd=socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
    if(sockfd==-1){
        fprintf(stderr, "socket err");
        return -1;
    }

    int val=-1;
    if(setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, (char*)&val, sizeof(val))<0){
        fprintf(stderr, "setsockopt err");
        close(sockfd);
        return -1;
    }

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* pcap= pcap_open_live(param.dev, BUFSIZ, 1, 1000, errbuf);

    if(pcap==NULL){
        fprintf(stderr, "pcap_open_live %s return null - %s", param.dev, errbuf);
        return -1;
    }

    struct pcap_pkthdr* header;
    const u_char* packet;
    http_pkt_pos* h;

    uint8_t my_mac[MAC_ADDR_LEN];
    get_mac_addr(string(param.dev), my_mac);

    bool is_https=false;

    while(true){

        int res= pcap_next_ex(pcap, &header,&packet);
        if(!res) continue;
        if(res==PCAP_ERROR || res== PCAP_ERROR_BREAK){
            printf("pcap_next_ex return %d , %s\n", res, pcap_geterr(pcap));
            break;
        }

        if(!parse_if_http_malicious(h, packet, header->caplen)) continue;
        http_pkt* forward_pkt;
        http_pkt* backward_pkt;
        make_forward_pkt(forward_pkt, h, my_mac, header->caplen);
        make_backward_pkt(backward_pkt, h, my_mac, is_https, header->caplen);

        res=pcap_sendpacket(pcap, reinterpret_cast<const u_char*>(forward_pkt), sizeof(*forward_pkt)-strlen(redirect_to_warning));
        if (res != 0) {
             fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(pcap));
        }

        res=send_rawsocket(backward_pkt, sockfd);

        /*
        res=pcap_sendpacket(pcap, reinterpret_cast<const u_char*>(backward_pkt), sizeof(*backward_pkt));
        if (res != 0) {
             fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(pcap));
        }
        */
    }

    pcap_close(pcap);

    return 0;
}
