

all: tcp-block

main.o: main.cpp

tcp-block: main.o
	g++ -o tcp-block main.o -lpcap

clean:
	rm -f tcp-block